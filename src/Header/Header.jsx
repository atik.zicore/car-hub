import React from "react";
import {
  FaCarSide,
  FaArrowRight,
  FaMapLocationDot,
  FaPhone,
  FaEnvelope,
} from "react-icons/fa6";
import { LiaCarSideSolid } from "react-icons/lia";
import { LuCodesandbox } from "react-icons/lu";
import { IoCarOutline } from "react-icons/io5";

import { FaQuoteLeft } from "react-icons/fa";
import { IoIosStar, IoIosSend } from "react-icons/io";

const App = () => {
  return (
    <div className="bg-gray-100 min-h-screen sm:h-max relative">
      {/* Header */}
      <header className="bg-white shadow-md h-20 sticky z-10 top-0">
        <div className="w-11/12 mx-auto  py-4 flex items-center justify-between">
          {/* Logo */}
          <div className="flex items-center text-red-500 text-xl">
            <FaCarSide size={20} />

            <span className="ml-2 font-bold text-lg">TNC CARHUB</span>
          </div>
          {/* Menu Items */}
          <nav className="hidden md:flex items-center space-x-4">
            <a href="#" className="text-gray-500 hover:text-red-400">
              Home
            </a>
            <a href="#" className="text-gray-500 hover:text-red-400">
              About Us
            </a>
            <a href="#" className="text-gray-500 hover:text-red-400">
              Vehicles
            </a>
            <a href="#" className="text-gray-500 hover:text-red-400">
              Gallery
            </a>
            <a href="#" className="text-gray-500 hover:text-red-400">
              Testimonials
            </a>
            <a href="#" className="text-gray-500 hover:text-red-400">
              Bloges
            </a>
            <a href="#" className="text-gray-500 hover:text-red-400">
              Pages
            </a>
            <button className="bg-red-500 text-white px-4 py-2 rounded-md hover:bg-orange-400 transition hover:duration-500">
              Contact Us
            </button>
          </nav>
        </div>
      </header>

      {/* Slider */}

      <div className="relative h-[35rem] sm:max-h-screen overflow-hidden flex justify-start items-center">
        <img
          src="https://placehold.co/600x400"
          alt="Slider"
          className="w-full h-full object-cover absolute z-0 inset-0"
        />
        <div
          className="relative z-10  py-10 
          flex flex-col items-center  justify-center sm:text-start sm:items-start sm:px-2 sm:py-14 bg-gradient-to-r from-gray-900 to-gray-50/0 bg-opacity-50
           text-white 
            sm:w-1/2"
        >
          <h1 className="text-7xl sm:text-4xl  font-semibold text-wrap ">
            Meet the New Stars of Our Showroom
          </h1>
          <p className="text-gray-300">
            Your Partner in Real Estate Success. Our mission is to help you find
            the perfect property or sell your current one with ease.
          </p>
          <div className="flex items-center bg-red-600 text-white px-8 gap-2 py-4 rounded-md ">
            <button className=" text-white ">Buy This Car</button>
            <span className="ml-2 text-white">
              <FaArrowRight />
            </span>
          </div>
        </div>
      </div>

      {/* Card */}
      <div className="card-container py-4 px-4 sm:p-4">
        <div className="grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-4 mx-auto gap-4 ">
          <div className="flex flex-col items-center justify-center bg-neutral-200 p-6 gap-2 w-full ">
            {/* <img src="https://placehold.co/400" alt="" className="object-cover w-24 h-24 rounded-full"/> */}
            <span>
              <IoCarOutline className="text-red-500 w-12 h-12" />
            </span>
            <h2 className="text-xl">Find a car</h2>
            <p>
              We provide a platform to help you find the perfect vehicle your
              preferenceslorem.
            </p>
          </div>

          <div className="flex flex-col items-center justify-center gap-2 bg-neutral-200 p-6">
            <LuCodesandbox className="w-12 h-12 text-red-500" />
            <h2 className="text-xl">Pre Order</h2>
            <p>
              We provide a platform to help you find the perfect vehicle your
              preferences.
            </p>
          </div>

          <div className="flex flex-col items-center justify-center gap-2 bg-neutral-200 p-6">
            <LuCodesandbox className="w-12 h-12 text-red-500" />
            <h2 className="text-xl">Latest Car</h2>
            <p>
              We provide a platform to help you find the perfect vehicle your
              preferences.
            </p>
          </div>

          <div className="flex flex-col items-center gap-2 bg-neutral-200 p-6">
            <img
              src="https://placehold.co/400"
              alt=""
              className="object-cover w-24 h-24 rounded-full"
            />
            <h2 className="text-xl">Used Car</h2>
            <p>
              We provide a platform to help you find the perfect vehicle your
              preferences.
            </p>
          </div>
        </div>
      </div>

      {/* About Section */}
      <div className="about-section grid grid-cols-1 md:grid-cols-2 gap-4 py-8 overflow-hidden ">
        <div className="">
          {/* card image */}
          <img
            src="https://placehold.co/1600x800"
            alt="car image"
            className="object-cover h-full w-full"
          />
        </div>
        <div className="text-start p-4 space-y-8 bg-neutral-200  pb-10 flex flex-col justify-center items-start">
          <h1 className="text-2xl sm:text-3xl lg:text-5xl font-semibold">
            About TNC CARHUB
          </h1>
          <p className="text-gray-600">
            TNC Carhub is your gateway to an exceptional automotive experience.
            As a premier destination for car enthusiasts and buyers, we are
            committed to redefining how you shop for and interact with cars.
          </p>
          <button className="bg-red-600 px-8 py-3 text-start text-white font-semibold rounded-md">
            Contact Us
          </button>
        </div>
      </div>

      {/* Featured Car */}

      <div className="featured-car relative overflow-hidden py-6 ">
        <div className="grid grid-cols-1 items-center md:grid-flow-row md:grid-rows-2 justify-center">
          <div className="">
            <h1 className="text-5xl font-semibold">Featured Car</h1>
            <p className="text-gray-500">
              Lorem ipsum dolor, sit amet consectetur adipisicing elit. Facilis
              ea amet, doloremque numquam beatae itaque minus labore sapiente
              culpa qui nobis tempora est eaque ab fugiat, voluptate quam!
              Beatae, laudantium?
            </p>
          </div>

          <div className=" sm:gap-8  content-center mx-auto justify-center w-full md:flex md:gap-1">
            <div className="flex flex-col items-center justify-center md:flex-row gap-2 md:gap-1">
              <img
                src="https://placehold.co/600x400"
                alt=""
                className="object-cover rounded-md max-w-full md:h-80 md:w-full "
              />

              <img
                src="https://placehold.co/600x400"
                alt=""
                className="object-cover rounded-md max-w-full md:h-80 md:w-full "
              />
              <img
                src="https://placehold.co/600x400"
                alt=""
                className="object-cover rounded-md max-w-full md:h-80 md:w-full "
              />
            </div>
          </div>
        </div>
      </div>

      {/* Car Search Today */}

      <div className="max-h-screen relative ">
        <img
          src="https://placehold.co/1920x400"
          className="object-cover h-full py-6w-full w-full absolute z-0 inset-0"
          alt=""
        />
        <div className="relative flex items-start p-4 justify-center md:items-center lg:items-center  flex-col py-6 gap-y-8">
          <h1 className="text-3xl ">Start Your Car Search Today</h1>
          <p className="text-sm  text-gray-700 ">
            Embarking on the journey to find your dream car is an exciting
            adventure, and it all begins when you start your car search today.
          </p>
          <button className="bg-red-600 px-5 py-3 items-center rounded-sm text-white font-semibold">
            Contact With Us
          </button>
        </div>
      </div>

      {/* Testimonial Area */}

      <div className="testimonial-section relative overflow-hidden py-8 p-4 space-y-6">
        <div className="testimonial-header">
          <h1 className="text-2xl w-3/4 mx-auto  md:text-3xl">
            All-Terrain Adventures Await: Browse Our Collection of Rugged
            Off-Road Vehicles.
          </h1>
        </div>

        {/* Testimonial Card */}
        <div className="testimonial-card grid grid-cols-1 md:grid-cols-2 lg:grid-cols-4 text-wrap text-justify gap-4">
          <div className="card-one space-x-2 space-y-4 h-full ">
            <div className="border p-6 rounded-md space-y-6 border-gray-300  ">
              <span>
                <FaQuoteLeft size={20} />
              </span>
              <p>
                I've purchased two cars through Car Hub, and both times the
                experience was top-notch. The variety of cars they offer, along
                with the helpful staff.
              </p>
              <div className="flex gap-1">
                <span>
                  <IoIosStar className="w-6 h-6 text-yellow-300" />
                </span>
                <span>
                  <IoIosStar className="w-6 h-6 text-yellow-300" />
                </span>
                <span>
                  <IoIosStar className="w-6 h-6 text-yellow-300" />
                </span>
                <span>
                  <IoIosStar className="w-6 h-6 text-yellow-300" />
                </span>
                <span>
                  <IoIosStar className="w-6 h-6 text-yellow-300" />
                </span>
              </div>
            </div>
            <div className="testimonial-author">
              <img
                src="https://placehold.co/40x40"
                alt=""
                className="object-cover rounded-full"
              />
              <div>
                <h2 className="text-xl">John Doe</h2>
                <p className="text-gray-500">Buyer</p>
              </div>
            </div>
          </div>

          <div className="card-one space-x-2 space-y-4 h-full ">
            <div className="border p-6 rounded-md space-y-6 border-gray-300 ">
              <span>
                <FaQuoteLeft size={20} />
              </span>
              <p>
                I've purchased two cars through Car Hub, and both times the
                experience was top-notch. The variety of cars they offer, along
                with the helpful staff.
              </p>
              <div className="flex gap-1">
                <span>
                  <IoIosStar className="w-6 h-6 text-yellow-300" />
                </span>
                <span>
                  <IoIosStar className="w-6 h-6 text-yellow-300" />
                </span>
                <span>
                  <IoIosStar className="w-6 h-6 text-yellow-300" />
                </span>
                <span>
                  <IoIosStar className="w-6 h-6 text-yellow-300" />
                </span>
                <span>
                  <IoIosStar className="w-6 h-6 text-yellow-300" />
                </span>
              </div>
            </div>
            <div className="testimonial-author">
              <img
                src="https://placehold.co/40x40"
                alt=""
                className="object-cover rounded-full"
              />
              <div>
                <h2 className="text-xl">John Doe</h2>
                <p className="text-gray-500">Buyer</p>
              </div>
            </div>
          </div>

          <div className="card-one space-x-2 space-y-4 h-full ">
            <div className="border p-6 rounded-md space-y-6 border-gray-300 ">
              <span>
                <FaQuoteLeft size={20} />
              </span>
              <p>
                I've purchased two cars through Car Hub, and both times the
                experience was top-notch. The variety of cars they offer, along
                with the helpful staff.
              </p>
              <div className="flex gap-1">
                <span>
                  <IoIosStar className="w-6 h-6 text-yellow-300" />
                </span>
                <span>
                  <IoIosStar className="w-6 h-6 text-yellow-300" />
                </span>
                <span>
                  <IoIosStar className="w-6 h-6 text-yellow-300" />
                </span>
                <span>
                  <IoIosStar className="w-6 h-6 text-yellow-300" />
                </span>
                <span>
                  <IoIosStar className="w-6 h-6 text-yellow-300" />
                </span>
              </div>
            </div>
            <div className="testimonial-author">
              <img
                src="https://placehold.co/40x40"
                alt=""
                className="object-cover rounded-full"
              />
              <div>
                <h2 className="text-xl">John Doe</h2>
                <p className="text-gray-500">Buyer</p>
              </div>
            </div>
          </div>

          <div className="card-one space-x-2 space-y-4 h-full ">
            <div className="border p-6 rounded-md space-y-6 border-gray-300 ">
              <span>
                <FaQuoteLeft size={20} />
              </span>
              <p>
                I've purchased two cars through Car Hub, and both times the
                experience was top-notch. The variety of cars they offer, along
                with the helpful staff.
              </p>
              <div className="flex gap-1">
                <span>
                  <IoIosStar className="w-6 h-6 text-yellow-300" />
                </span>
                <span>
                  <IoIosStar className="w-6 h-6 text-yellow-300" />
                </span>
                <span>
                  <IoIosStar className="w-6 h-6 text-yellow-300" />
                </span>
                <span>
                  <IoIosStar className="w-6 h-6 text-yellow-300" />
                </span>
                <span>
                  <IoIosStar className="w-6 h-6 text-yellow-300" />
                </span>
              </div>
            </div>
            <div className="testimonial-author">
              <img
                src="https://placehold.co/40x40"
                alt=""
                className="object-cover rounded-full"
              />
              <div>
                <h2 className="text-xl">John Doe</h2>
                <p className="text-gray-500">Buyer</p>
              </div>
            </div>
          </div>
        </div>
      </div>

      {/* Latest News */}
      <div className="latestNews-area relative  py-4">
        <div className="latestNews-header ">
          <h1 className="text-2xl sm:text-3xl md:text-4xl pb-8">Latest News</h1>
          <p className="text-gray-600 pb-8 px-2">
            Stay up-to-date with the latest happenings in the automotive world
            at Car Hub. Discover what's new and exciting in the world of
            automobiles at Car Hub.
          </p>
        </div>
        {/* Latest News BOdy */}
        <div className="latestNews-body grid grid-cols-1 lg:grid-cols-2 gap-6  p-4">
          {/* Latest News Card One */}
          <div className="md:flex gap-2 bg-gray-200 rounded-md shadow-md p-4 hover:bg-black hover:text-white duration-500">
            {/* latestNews Image */}
            <div className="latestNews-image md:w-1/2 p-1">
              <img
                src="https://placehold.co/600x400"
                alt=""
                className="object-cover h-full w-full rounded-xl"
              />
            </div>
            {/* latest News Content */}
            <div className="latestNews-content md:w-1/2 p-4 py-8">
              <div className="flex flex-col gap-4 text-start">
                <button className="bg-red-600  text-wrap text-white w-48 py-2 rounded-md">
                  Auto Industry News
                </button>
                <h1 className="text-2xl">
                  The Impact of Car Technology on the Automotive Industry
                </h1>
                <p className="text-gray-500">
                  Explore the dynamic relationship between automotive technology
                  and the industry's evolution.
                </p>
                <button className="flex gap-x-2 items-center text-gray-500">
                  Read More{" "}
                  <span>
                    <FaArrowRight className="w-3 h-3" />
                  </span>
                </button>
              </div>
            </div>
          </div>
          {/* Latest News Card Two */}
          <div className="md:flex gap-2 bg-gray-200 rounded-md shadow-md">
            {/* latestNews Image */}
            <div className="latestNews-image md:w-1/2  p-2">
              <img
                src="https://placehold.co/600x400"
                alt=""
                className="object-cover h-full w-full rounded-xl"
              />
            </div>
            {/* latest News Content */}
            <div className="latestNews-content md:w-1/2 p-4 py-8">
              <div className="flex flex-col gap-4 text-start">
                <button className="bg-red-600  text-wrap text-white w-48 py-2 rounded-md">
                  Auto Industry News
                </button>
                <h1 className="text-2xl">
                  The Impact of Car Technology on the Automotive Industry
                </h1>
                <p className="text-gray-500">
                  Explore the dynamic relationship between automotive technology
                  and the industry's evolution.
                </p>
                <button className="flex gap-x-2 items-center text-gray-500">
                  Read More{" "}
                  <span>
                    <FaArrowRight className="w-3 h-3" />
                  </span>
                </button>
              </div>
            </div>
          </div>
          {/* Latest News Card Three */}
          <div className="md:flex gap-2 bg-gray-200 rounded-md shadow-md">
            {/* latestNews Image */}
            <div className="latestNews-image md:w-1/2 p-4">
              <img
                src="https://placehold.co/600x400"
                alt=""
                className="object-cover h-full w-full rounded-xl"
              />
            </div>
            {/* latest News Content */}
            <div className="latestNews-content md:w-1/2 p-4 py-8">
              <div className="flex flex-col gap-4 text-start">
                <button className="bg-red-600  text-wrap text-white w-48 py-2 rounded-md">
                  Auto Industry News
                </button>
                <h1 className="text-2xl">
                  The Impact of Car Technology on the Automotive Industry
                </h1>
                <p className="text-gray-500">
                  Explore the dynamic relationship between automotive technology
                  and the industry's evolution.
                </p>
                <button className="flex gap-x-2 items-center text-gray-500">
                  Read More{" "}
                  <span>
                    <FaArrowRight className="w-3 h-3" />
                  </span>
                </button>
              </div>
            </div>
          </div>

          {/* Latest News Card Four */}
          <div className="md:flex gap-2 bg-gray-200 rounded-md shadow-md">
            {/* latestNews Image */}
            <div className="latestNews-image md:w-1/2 p-4">
              <img
                src="https://placehold.co/600x400"
                alt=""
                className="object-cover h-full w-full rounded-xl"
              />
            </div>
            {/* latest News Content */}
            <div className="latestNews-content md:w-1/2 p-4 py-8">
              <div className="flex flex-col gap-4 text-start">
                <button className="bg-red-600  text-wrap text-white w-48 py-2 rounded-md">
                  Auto Industry News
                </button>
                <h1 className="text-2xl">
                  The Impact of Car Technology on the Automotive Industry
                </h1>
                <p className="text-gray-500">
                  Explore the dynamic relationship between automotive technology
                  and the industry's evolution.
                </p>
                <button className="flex gap-x-2 items-center text-gray-500">
                  Read More{" "}
                  <span>
                    <FaArrowRight className="w-3 h-3" />
                  </span>
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>

      {/* Subscribe Area */}

      <div className="relative h-[15rem] subscriber-area  py-8 ">
        <div>
          <img
            src="https://placehold.co/1920x300"
            alt=""
            className="object-cover absolute w-full h-full z-0 inset-0"
          />
          <div className="relative justify-center items-center z-5">
            <h1 className="text-2xl py-4">Subscribe and save today!</h1>
            <form  action="" className="relative w-96 mx-auto">
              <input
                type="email"
                placeholder="email@email.com"
                className="rounded-3xl px-2 py-2 outline-none  focus:outline-blue-500  w-full relative z-0"
                required
              />{" "} 
              <IoIosSend className="w-5 h-5 text-red-500 absolute z-10 right-3 top-3" />
              {/* <input type="submit" /> */}
            </form>
          </div>
        </div>
      </div>

      {/* Footer Top Menu */}
      <div className="footer-menu relative py-6">
        <div className="grid grid-cols-1 lg:grid-cols-3 items-start gap-4 p-4 text-start">
          <div className="p-4 shadow-md h-full">
            <h2 className="text-2xl py-4">Showroom Address:</h2>

            <div className="flex items-center gap-x-4">
              <span>
                <FaMapLocationDot />
              </span>
              <p className="text-gray-600">
                4517 Washington Ave. Manchester, Kentucky 39495.
              </p>
            </div>

            <div className="flex items-center gap-x-4">
              <span>
                <FaPhone />
              </span>
              <p className="text-gray-600">(406) 555-0120</p>
            </div>

            <div className="flex items-center gap-x-4">
              <span>
                <FaEnvelope />
              </span>
              <p className="text-gray-600">example@gmail.com</p>
            </div>

            <div className="flex items-center gap-x-4">
              <span>
                <FaMapLocationDot />
              </span>
              <p className="text-gray-600">Lorem ipsum dolor sit amet.</p>
            </div>
          </div>

          <div className="p-4 shadow-md  h-full">
            <div className="flex justify-between text-start">
              <div>
                <h4 className="text-xl">Pages</h4>
                <nav className="py-6">
                  <ul className="">
                    <li>
                      <a href="" className="text-gray-500 hover:text-red-400">
                        Home
                      </a>
                    </li>
                    <li>
                      <a href="" className="text-gray-500 hover:text-red-400">
                        About
                      </a>
                    </li>
                    <li>
                      <a href="" className="text-gray-500 hover:text-red-400">
                        Blogs
                      </a>
                    </li>
                    <li>
                      <a href="" className="text-gray-500 hover:text-red-400">
                        Gallery
                      </a>
                    </li>
                    <li>
                      <a href="" className="text-gray-500 hover:text-red-400">
                        Testimonials
                      </a>
                    </li>
                  </ul>
                </nav>
              </div>
              <div>
                <h4 className="text-xl">Find a car</h4>
                <nav className="py-6">
                  <ul className="">
                    <li>
                      <a href="" className="text-gray-500 hover:text-red-400">
                        New Cars
                      </a>
                    </li>
                    <li>
                      <a href="" className="text-gray-500 hover:text-red-400">
                        Car Brands
                      </a>
                    </li>
                    <li>
                      <a href="" className="text-gray-500 hover:text-red-400">
                        404
                      </a>
                    </li>
                    <li>
                      <a href="" className="text-gray-500 hover:text-red-400">
                        Contact
                      </a>
                    </li>
                    <li>
                      <a href="" className="text-gray-500 hover:text-red-400">
                        Licesing
                      </a>
                    </li>
                  </ul>
                </nav>
              </div>
              <div>
                <h4 className="text-xl">Help</h4>
                <nav className="py-6">
                  <ul className="">
                    <li>
                      <a href="" className="text-gray-500 hover:text-red-400">
                        About Us
                      </a>
                    </li>
                    <li>
                      <a href="" className="text-gray-500 hover:text-red-400">
                        Style-Guide
                      </a>
                    </li>
                    <li>
                      <a href="" className="text-gray-500 hover:text-red-400">
                        Change-Log
                      </a>
                    </li>
                    <li>
                      <a href="" className="text-gray-500 hover:text-red-400">
                        Contact
                      </a>
                    </li>
                  </ul>
                </nav>
              </div>
            </div>
          </div>

          <div className="p-4 shadow-md h-full">
            <h2 className="text-xl">Business Partner</h2>
            <div className="grid grid-cols-4 gap-4 py-8 items-center justify-between flex-wrap">
              <img
                src="https://placehold.co/80x80"
                alt=""
                className="object-cover "
              />
              <img
                src="https://placehold.co/80x80"
                alt=""
                className="object-cover "
              />
              <img
                src="https://placehold.co/80x80"
                alt=""
                className="object-cover "
              />
              <img
                src="https://placehold.co/80x80"
                alt=""
                className="object-cover "
              />
              <img
                src="https://placehold.co/80x80"
                alt=""
                className="object-cover "
              />
              <img
                src="https://placehold.co/80x80"
                alt=""
                className="object-cover "
              />
              <img
                src="https://placehold.co/80x80"
                alt=""
                className="object-cover "
              />
              <img
                src="https://placehold.co/80x80"
                alt=""
                className="object-cover "
              />
            </div>
          </div>
        </div>
      </div>

      {/* Copyright */}
      <div>
        <div>
          <div className="copyright-content text-gray-500 text-xs">
            <p>Copyright &copy; 2024 | Designed by Team Zicore.</p>
            <p>Powered by Zicore</p>
          </div>
          <div className="copyright-image flex justify-center gap-4 py-4">
            <img src="https://placehold.co/40x40" alt="" />
            <img src="https://placehold.co/40x40" alt="" />
            <img src="https://placehold.co/40x40" alt="" />
            <img src="https://placehold.co/40x40" alt="" />
          </div>
        </div>
      </div>
    </div>
  );
};

export default App;
